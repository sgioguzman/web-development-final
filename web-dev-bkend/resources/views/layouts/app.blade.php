<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Rolling Partner</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ asset('../css/app.css') }}" rel="stylesheet">
</head>
<body id='hero'>
    <section id="header">
        <div class="header container">
            <div class="nav-bar">
                <div class="brand">
                    <a href="{{ url('/') }}" id='small-brand'>
                        <h1><span>Rolling</span> Partner</h1>
                    </a>
                </div>
                <div class="nav-list">
                    <ul>
                        @guest
                            <li><a href="{{ url('/') }}" data-after="Home">Home</a></li>
                            @if (Route::has('login'))
                                <li><a href="{{ route('login') }}" data-after="Sign In">{{ __('Login') }}</a></li>
                            @endif
                            @if (Route::has('register'))
                                <li><a href="{{ route('register') }}" data-after="Sign Up">{{ __('Register') }}</a></li>
                            @endif
                        @else
                                <li><a data-after="User" href="#">
                                {{ __('Welcome! ') }} {{ Auth::user()->username }}</a></li>
                                <li><a href="{{ url('/') }}" data-after="Home">Home</a></li>
                                <li><a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}</a></li>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <main>
        @yield('content')
    </main>
</body>
</html>
