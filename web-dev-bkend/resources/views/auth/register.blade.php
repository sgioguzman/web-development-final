@extends('layouts.app')

@section('content')
<section id='login-page'>
    <div class="overlay">
        <form class="standard" method="POST" action="{{ route('register') }}">
            @csrf
            <div class="con">
                <header class="head-form">
                    <h2>Sign Up</h2>
                        <!--     A welcome message or an explanation of the login form -->
                    <p>Welcome! Please fill out the fields to create an account.</p>
                </header>
                <br>
                <div class="field-set">
                    <!--firstName-->
                    <input class="form-input-reg form-control @error('first_name') is-invalid @enderror" id="first_name" type="text" name="first_name" placeholder="First Name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>
                    @error('first_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <br>
                    <!--LastName-->
                    <input class="form-input-reg form-control @error('last_name') is-invalid @enderror" id="last_name" type="text" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>
                    @error('last_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <br>
                    <!--Email-->
                    <input id="email" type="email" class="form-input-reg form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email" required autocomplete="email">
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <br>
                    <!--Username-->
                    <input id="username" type="text" class="form-input-reg form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" placeholder="Username" required autocomplete="username" autofocus/>
                    @error('username')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <br>
                    <!--Password-->
                    <input id="password" type="password" class="form-input-reg form-control @error('password') is-invalid @enderror" name="password"  placeholder="Password" required autocomplete="new-password">
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <br>
                    <!--Confirm Password-->
                        <input id="password-confirm" type="password" class="form-input-reg form-control" name="password_confirmation" placeholder="Confirm Password" required autocomplete="new-password">
                    <br>
                    <!--Button-->
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-reg">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>           
    </div>
</section>
@endsection
