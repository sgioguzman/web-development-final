@extends('layouts.app')

@section('content')
<section id='login-page'>
    <div class="overlay">
        <form  class="standard" method="POST" action="{{ route('login') }}">
            @csrf
                <div class="con">
                    <header class="head-form">
                        <h2>Sign in</h2>
                            <!--     A welcome message or an explanation of the login form -->
                        <p>Login here using your username and password</p>
                    </header>
                    <br>
                    <div class="field-set">
                        <!--   user name -->
                        <!--   user name Input-->
                        <input class="form-input-usr form-control @error('username') is-invalid @enderror" id="username" type="text" name="username" placeholder="Username" value="{{ old('username') }}" required autocomplete="username">
                        @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <br>

                        <!--   Password -->
                        <!--   Password Input-->
                        <input class="form-input form-control @error('password') is-invalid @enderror" type="password" placeholder="Password" id="password"  name="password" required autocomplete="current-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        <!--      Show/hide password  -->
                        <span>
                            <i class="fa fa-eye" aria-hidden="true"  type="button" id="eye"><img src="../images/icons8-eye-16.png"></i>
                        </span>
                        <br>

                        <!--        buttons -->
                        <!--      button LogIn -->
                        <button class="log-in btn btn-primary" type="submit"> 
                            {{ __('Login') }} 
                        </button>
                    </div>
                        <!--      Remember Me -->
                    <div>
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                        <!--   other buttons -->
                    <div class="other">
                        <!--      Forgot Password button-->
                        <button class="btn submits frgt-pass">
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Password?') }}
                            </a>
                        </button>
                        <!--     Sign Up button -->
                        <button class="btn submits sign-up" >
                            <a class="btn btn-link" href="{{ route('register') }}">
                                    {{ __('Sign Up') }}
                            </a> 
                        </button>
                    <!--      End Other the Division -->
                    </div>  
                    <!--   End Conrainer  -->
                </div>            
        <!-- End Form -->
        </form>
    </div>
    <script src="{{ asset('js/logscript.js') }}"></script>
</section>
@endsection
