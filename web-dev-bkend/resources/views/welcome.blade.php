<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
  <title>Rolling Partner</title>
</head>

<body>
  <!-- Header -->
  <section id="header">
    <div class="header container">
      <div class="nav-bar">
        <div class="brand">
          <a href="#hero">
            <h1><span>Rolling</span> Partner</h1>
          </a>
        </div>
        <div class="nav-list">
          <div class="hamburger">
            <div class="bar"></div>
          </div>
          <ul>
            <li><a href="#hero" data-after="Home">Home</a></li>
            <li><a href="#services" data-after="Service">Services</a></li>
            <li><a href="#about" data-after="About">About</a></li>
            <li><a href="#contact" data-after="Contact">Contact</a></li>
            @if (Route::has('login'))
                @auth
                    <li><a href="{{ url('/home') }}" data-after="Home">My Account</a></li>
                @else
                    <li><a href="{{ route('login') }}" data-after="Sign In">Login</a></li>

                    @if (Route::has('register'))
                    <li><a href="{{ route('register') }}" data-after="Sign Up">Register</a></li>
                    @endif
                @endauth
              </div>
            @endif
            
            
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!-- End Header -->


  <!-- Hero Section  -->
  <section id="hero">
    <div class="hero container">
      <div>
        <h1>Hi Everyone, <span></span></h1>
        <h1>We Are <span></span></h1>
        <h1>Rolling Partner <span></span></h1>
        <a href="#services" type="button" class="cta">our services</a>
      </div>
    </div>
  </section>
  <!-- End Hero Section  -->

  <!-- Service Section -->
  <section id="services">
    <div class="services container">
      <div class="service-top">
        <h1 class="section-title">our <span>premise</span></h1>
        <p>
            To put it simple we love cycling. So if you love it too, then we are the right fit for all of 
            your security needs. Through our GPS system we will be able to provide you with a full suite
            of feature so that we can ride alongside with you, while not being physically there. 
            <p class="our-products">Our suite of services are:</p> 
        </p>
      </div>
      <div class="service-bottom">
        <div class="service-item gps-ser">
          <div class="icon"><img src="https://img.icons8.com/officel/80/000000/worldwide-location--v1.png"/></div>
          <h2>Real Time Location</h2>
          <p>
                Our services will allow you to locate your Bike in real time. Thanks to our GPS you will be able 
                to locate your precious iron horse at any time, all you need is a device with internet conection,
                and access to your account.
          </p>
        </div>
        <div class="service-item route-ser">
          <div class="icon"><img src="https://img.icons8.com/cotton/64/000000/taxi-route--v1.png"/></div>
          <h2>Route Creation</h2>
          <p>
                Before each ride you will be able to create a customized route through our mobile app or our website. 
                This will allow you to set specific points, a start and a destination. With this you will be able
                to enjoy all the safety services we provide.
          </p>
        </div>
        <div class="service-item emergency-ser">
          <div class="icon"><img src="https://img.icons8.com/dusk/64/000000/hospital-2.png"/></div>
          <h2>Medical Assistance</h2>
          <p>
                We offer an ambulance service in case something happens during your ride. You can request this service
                through the app, or through an emergency flow previously created. This is our way to keep you safe
                during these long adventures.
          </p>
        </div>
        <div class="service-item police-ser">
          <div class="icon"><img src="https://img.icons8.com/officel/80/000000/policeman-male.png"/></div>
          <h2>Police Assistance</h2>
          <p>
                Sometimes things are out of our hands, and we may end up in a situation where we need
                to give up our valuables in order to keep us safe. this is the reason why we have direct contact with local the local police, 
                and have specials protocols to maximize the efficiency of the services.
            </p>
        </div>
        <div class="service-item flow-ser">
            <div class="icon"><img src="https://img.icons8.com/dusk/64/000000/flow-chart.png"/></div>
            <h2>Emergency Flow</h2>
            <p>
                This will allow you to create your own emergency flows. When you go out for a ride, our support team will
                look at your location, and trigger a Medical call or Police call if needed. We aim to act as fast as possible
                based on the flow you have created.
            </p>
          </div>
        <div class="service-item support-ser">
            <div class="icon"><img src="https://img.icons8.com/cotton/64/000000/online-support.png"/></div>
            <h2>Support</h2>
            <p>
                We will have a support team always aiming to fix any issues with the platform, and service. However, we also have
                a team that will follow you along your ride through the GPS system. That way we make sure that everything is going
                as you expected.
            </p>
          </div>
      </div>
    </div>
  </section>
  <!-- End Service Section -->

  <!-- About Section -->
  <section id="about">
    <div class="about container">
      <div class="col-left">
        <div class="about-img">
          <img src="../images/myself.png" alt="img">
        </div>
      </div>
      <div class="col-right">
        <h1 class="section-title">About <span>me</span></h1>
        <h2>Junior Software Developer / Support Engineer</h2>
        <p>I am a Support Engineer who has been in this industry for about 6 years. I've always been working with client
            aiming to make their dreams come true. Recently I have started as a Software Developer working in small personal projects
            and I am aiming towards mixing my Support carrer with my passion for developing complete, and efficient solutions.
        </p>
        <a href="https://drive.google.com/uc?export=download&id=1Dp0y0aCcLRAbXUb4KnFqKIjGy-1d1mDI" class="cta">Download Resume</a>
      </div>
    </div>
  </section>
  <!-- End About Section -->

  <!-- Contact Section -->
  <section id="contact">
    <div class="contact container">
      <div>
        <h1 class="section-title">Contact <span>info</span></h1>
      </div>
      <div class="contact-items">
        <div class="contact-item">
          <div class="icon"><img src="https://img.icons8.com/bubbles/100/000000/phone.png" /></div>
          <div class="contact-info">
            <h1>Phone</h1>
            <h2>+57 322 786 7246</h2>
          </div>
        </div>
        <div class="contact-item">
          <div class="icon"><img src="https://img.icons8.com/bubbles/100/000000/new-post.png" /></div>
          <div class="contact-info">
            <h1>Email</h1>
            <h2>sgioguzman@gmail.com</h2>
          </div>
        </div>
        <div class="contact-item">
          <div class="icon"><img src="https://img.icons8.com/bubbles/100/000000/map-marker.png" /></div>
          <div class="contact-info">
            <h1>Address</h1>
            <h2>Bogota, Colombia</h2>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End Contact Section -->

  <!-- Footer -->
  <section id="footer">
    <div class="footer container">
      <div class="brand">
        <h1><span>R</span>olling <span>P</span>artner</h1>
      </div>
      <h2>Your Complete Ride Solution</h2>
      <div class="social-icon">
        <div class="social-item">
          <a href="#"><img src="https://img.icons8.com/bubbles/100/000000/facebook-new.png" /></a>
        </div>
        <div class="social-item">
          <a href="#"><img src="https://img.icons8.com/bubbles/100/000000/instagram-new.png" /></a>
        </div>
        <div class="social-item">
          <a href="#"><img src="https://img.icons8.com/bubbles/100/000000/twitter.png" /></a>
        </div>
        <div class="social-item">
          <a href="#"><img src="https://img.icons8.com/bubbles/100/000000/behance.png" /></a>
        </div>
      </div>
      <p>Copyright © 2022. All rights reserved</p>
    </div>
  </section>
  <!-- End Footer -->
  <script src="{{ asset('js/scripts.js') }}"></script>
</body>

</html>