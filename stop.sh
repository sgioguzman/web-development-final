#!/bin/bash
echo "Stopping Containers..."
docker stop Rolling-Partner
docker rm Rolling-Partner
docker rmi my-laravel-image
docker-compose down
